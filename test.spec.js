const { test, expect } = require('@playwright/test');

test('test', async ({ page }) => {
    // Go to https://www.wikipedia.org/
    await page.goto('https://www.wikipedia.org/');
    // Click input[name="search"]
    await page.click('input[name="search"]');
    // Fill input[name="search"]
    await page.fill('input[name="search"]', 'testc');
});
